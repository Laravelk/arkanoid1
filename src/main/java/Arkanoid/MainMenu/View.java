package Arkanoid.MainMenu;

import javax.swing.*;

// view of main game menu
public class View extends JFrame {
    public View() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(900, 600);
        setTitle("Arkanoid");
    }
}
